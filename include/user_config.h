#ifndef __USER_CONFIG_H__
#define __USER_CONFIG_H__

/*DEFAULT CONFIGURATIONS*/
#define CFG_LOCATION	0x7B 	// 0x7B	0x3C 0x7D/* Please don't change or if you know what you doing */

#define MQTT_HOST			"192.168.11.125" //or "mqtt.yourdomain.com"
#define MQTT_PORT			1883
#define MQTT_BUF_SIZE		1024
#define MQTT_KEEPALIVE		120	 /*second*/

#define MQTT_CLIENT_ID		"DVES_%08X"
#define MQTT_USER			"DVES_USER"
#define MQTT_PASS			"DVES_PASS"

#define STA_TYPE AUTH_WPA2_PSK

#define MQTT_RECONNECT_TIMEOUT 	5	/*second*/

#define DEFAULT_SECURITY	0
#define QUEUE_BUFFER_SIZE	1024

#define PROTOCOL_NAMEv31	/*MQTT version 3.1 compatible with Mosquitto v0.15*/

#endif
