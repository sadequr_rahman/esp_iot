/*
 * PMS303.cpp
 *
 *  Created on: Nov 17, 2016
 *      Author: APL-SadequrRahman
 */

#include "PMS3003Data.h"

#define  word(h,l) (((uint16_t)h<<8)| (l & 0xFF))

uint16_t ICACHE_FLASH_ATTR PMS3003Data::getPm010Atm() {
		return pm010_ATM;
	}

void ICACHE_FLASH_ATTR PMS3003Data::setPm010Atm(uint16_t pm010Atm) {
	pm010_ATM = pm010Atm;
}

uint16_t ICACHE_FLASH_ATTR PMS3003Data::getPm010Tsi() {
	return pm010_TSI;
}

void ICACHE_FLASH_ATTR PMS3003Data::setPm010Tsi(uint16_t pm010Tsi) {
	pm010_TSI = pm010Tsi;
}

uint16_t ICACHE_FLASH_ATTR PMS3003Data::getPm025Atm() {
	return pm025_ATM;
}

void ICACHE_FLASH_ATTR PMS3003Data::setPm025Atm(uint16_t pm025Atm) {
	pm025_ATM = pm025Atm;
}

uint16_t ICACHE_FLASH_ATTR PMS3003Data::getPm025Tsi() {
	return pm025_TSI;
}

void ICACHE_FLASH_ATTR PMS3003Data::setPm025Tsi(uint16_t pm025Tsi) {
	pm025_TSI = pm025Tsi;
}

uint16_t ICACHE_FLASH_ATTR PMS3003Data::getPm100Atm() {
	return pm100_ATM;
}

void ICACHE_FLASH_ATTR PMS3003Data::setPm100Atm(uint16_t pm100Atm) {
	pm100_ATM = pm100Atm;
}

uint16_t ICACHE_FLASH_ATTR PMS3003Data::getPm100Tsi() {
	return pm100_TSI;
}

void ICACHE_FLASH_ATTR PMS3003Data::setPm100Tsi(uint16_t pm100Tsi) {
	pm100_TSI = pm100Tsi;
}


uint16 ICACHE_FLASH_ATTR PMS3003Data::getTsi025Index(void){
	uint16 idx(0);
	idx = calculateAQI(this->pm025_TSI);
	if(idx >500)
	{
		idx = 500;
	}
	return idx;
}

void ICACHE_FLASH_ATTR PMS3003Data::updateModel(uint8_t *buf)
{
	setPm010Tsi(word(buf[2],buf[3]));
	setPm025Tsi(word(buf[4],buf[5]));
	setPm100Tsi(word(buf[6],buf[7]));
	setPm010Atm(word(buf[8],buf[9]));
	setPm025Atm(word(buf[10],buf[11]));
	setPm100Atm(word(buf[12],buf[13]));

}
