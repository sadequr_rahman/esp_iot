/*
 * PMS3003_Manager.cpp
 *
 *  Created on: Nov 20, 2016
 *      Author: APL-SadequrRahman
 */


#include "PMS3003_Manager.h"
#include "../macq/MACQ.h"

extern "C"{
#include "osapi.h"
#include "os_type.h"
#include "driver/uart.h"
#include "espmissingincludes.h"
#include "gpio.h"
#include "user_interface.h"

}

inline uint16 word(uint8 h,uint8 l){
	return (((uint16_t)h<<8)| (l & 0xFF));
}


#define SET_PIN		3
#define RESET_PIN	0

extern PMS3003_Manager pms3003Manager;
extern PMS3003Data pms3003Data;

LOCAL os_timer_t readBufTimer;
LOCAL uint8_t state;
LOCAL uint16_t _10msCounter = 0;
LOCAL MACQ pmSensorBuf;

LOCAL void readBufCallback(void *arg)
{
	_10msCounter++;
	if(_10msCounter%2000 == 0)
	{
		os_printf("Turing on sensor\r\n");
		GPIO_OUTPUT_SET(SET_PIN, 1);
	}
	if(_10msCounter%2200 == 0)
	{
		os_printf("Turing off sensor\r\n");
		GPIO_OUTPUT_SET(SET_PIN, 0);
	}


}

ICACHE_FLASH_ATTR
PMS3003_Manager::PMS3003_Manager(){

	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
	GPIO_OUTPUT_SET(SET_PIN, 1);
	GPIO_OUTPUT_SET(RESET_PIN,1);
	UART_SetBaudrate(UART0,BIT_RATE_9600);
	//os_printf("Creating pms3003 manager\r\n");
	//os_timer_disarm(&readBufTimer);
	//os_timer_setfn(&readBufTimer,(os_timer_func_t *)readBufCallback,(void*)0);
	//os_timer_arm(&readBufTimer,10,1);
}

ICACHE_FLASH_ATTR PMS3003_Manager::~PMS3003_Manager()
{
}


void ICACHE_FLASH_ATTR
PMS3003_Manager::parseAndUpdate(uint8_t *buf)
{
	//os_printf("Received Data Len-> %d\r\n",word(buf[0],buf[1]));
	pmSensorBuf.put(buf);
	pmSensorBuf.get(&pms3003Data);

	pms3003Manager.notify(&pms3003Data);

}


void ICACHE_FLASH_ATTR PMS3003_Manager::registerLisenter(PmsSensorInterface *obj)
{
	//os_printf("Adding to Event List\r\n");
	LisenterList.push_back(obj);
}

void ICACHE_FLASH_ATTR PMS3003_Manager::notify(PMS3003Data *dataObj)
{
	for (int i = 0; i < LisenterList.size(); i++)
		{
			//os_printf("Sending Notification\r\n");
			LisenterList[i]->update(dataObj);
		}
}

void ICACHE_FLASH_ATTR PMS3003_Manager::turnOn()
{
	GPIO_OUTPUT_SET(SET_PIN, 1);
}

void ICACHE_FLASH_ATTR PMS3003_Manager::turnOff()
{
	GPIO_OUTPUT_SET(SET_PIN, 0);
}
