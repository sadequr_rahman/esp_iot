/*
 * MqttManager.cpp
 *
 *  Created on: Nov 21, 2016
 *      Author: APL-SadequrRahman
 */

#include "MqttManager.h"

static MQTT_Client myclient;
static PMS3003Data *sensorData;


#ifdef __cplusplus
extern "C"
{
#endif
#include "osapi.h"
#include "mem.h"
#include "espmissingincludes.h"
#include "user_json.h"
#include "sntp.h"


LOCAL int ICACHE_FLASH_ATTR
pms3003_get(struct jsontree_context *js_ctx)
{
    const char *path = jsontree_path_name(js_ctx, js_ctx->depth - 1);

    if (os_strncmp(path, "deviceID", 8) == 0) {
    	uint8 macaddr[6];
		char buf[32];
		os_memset(buf,'\0',32);
		wifi_get_macaddr(STATION_IF,macaddr);
		os_sprintf(buf,MACSTR,macaddr[0],macaddr[1],macaddr[2],macaddr[3],macaddr[4],
				macaddr[5]);
		 jsontree_write_string(js_ctx, buf);
        }
    else if(os_strncmp(path, "AQI(US)", 7) == 0) {
    	jsontree_write_int(js_ctx, sensorData->getTsi025Index());

    	}
    else if(os_strncmp(path, "Time", 4) == 0) {
    	/*
    	uint32 current_stamp;
		current_stamp = sntp_get_current_timestamp();
    	char buf[64];
		os_memset(buf,'\0',64);
		os_strcpy(buf,sntp_get_real_time(current_stamp));
    	jsontree_write_string(js_ctx, buf );
    	*/
	}
    else if(os_strncmp(path, "pm010tsi", 8) == 0) {
    	 jsontree_write_int(js_ctx, sensorData->getPm010Tsi());
    }
    else if(os_strncmp(path, "pm025tsi", 8) == 0){
    	 jsontree_write_int(js_ctx, sensorData->getPm025Tsi());
    	}
    else if(os_strncmp(path, "pm100tsi", 8) == 0){
    	 jsontree_write_int(js_ctx, sensorData->getPm100Tsi());
        	}
    else if(os_strncmp(path, "pm010atm", 8) == 0){
    	 jsontree_write_int(js_ctx, sensorData->getPm010Atm());
           	}
    else if(os_strncmp(path, "pm025atm", 8) == 0){
    	 jsontree_write_int(js_ctx, sensorData->getPm025Atm());
           	}
    else if(os_strncmp(path, "pm100atm", 8) == 0){
    	 jsontree_write_int(js_ctx, sensorData->getPm100Atm());
           	}

    return 0;
}

LOCAL struct jsontree_callback pms3003_callback =
    JSONTREE_CALLBACK(pms3003_get, NULL);


#ifdef __cplusplus
}
#endif

LOCAL os_timer_t mqttSendF;
LOCAL bool sendFlag(false);
LOCAL uint8 timeCounter(0);


void ICACHE_FLASH_ATTR timerCallback(void *arg){
	timeCounter++;
	if(timeCounter >= 15){
		os_timer_disarm(&mqttSendF);
		timeCounter = 0;
		sendFlag = true;
	}
}


/*
LOCAL char pub_topic0[] = "pms3003/tsi/010";
LOCAL char pub_topic1[] = "pms3003/tsi/025";
LOCAL char pub_topic2[] = "pms3003/tsi/100";
LOCAL char pub_topic3[] = "pms3003/Atm/010";
LOCAL char pub_topic4[] = "pms3003/Atm/025";
LOCAL char pub_topic5[] = "pms3003/Atm/100";
*/



JSONTREE_OBJECT(sensorTree,
				JSONTREE_PAIR("deviceID", &pms3003_callback),
				JSONTREE_PAIR("Time", &pms3003_callback),
				JSONTREE_PAIR("AQI(US)", &pms3003_callback),
                JSONTREE_PAIR("pm010tsi", &pms3003_callback),
                JSONTREE_PAIR("pm025tsi", &pms3003_callback),
                JSONTREE_PAIR("pm100tsi", &pms3003_callback),
                JSONTREE_PAIR("pm010atm", &pms3003_callback),
                JSONTREE_PAIR("pm025atm", &pms3003_callback),
				JSONTREE_PAIR("pm100atm", &pms3003_callback)
                );

JSONTREE_OBJECT(sensorinfo_tree,JSONTREE_PAIR("sensor_info",&sensorTree));



ICACHE_FLASH_ATTR char *ultostr(unsigned long value, char *ptr, int base)
{
  unsigned long t = 0, res = 0;
  unsigned long tmp = value;
  int count = 0;

  if (NULL == ptr)
  {
    return NULL;
  }

  if (tmp == 0)
  {
    count++;
  }

  while(tmp > 0)
  {
    tmp = tmp/base;
    count++;
  }

  ptr += count;

  *ptr = '\0';

  do
  {
    res = value - base * (t = value / base);
    if (res < 10)
    {
      * -- ptr = '0' + res;
    }
    else if ((res >= 10) && (res < 16))
    {
        * --ptr = 'A' - 10 + res;
    }
  } while ((value = t) != 0);

  return(ptr);
}


ICACHE_FLASH_ATTR MqttManager::MqttManager()
{
	//os_sprintf(timeS, "%02d:%02d:%02d", 0, 0, 0);
	//os_printf("Creating mqtt object\r\n");
	MQTT_InitConnection(&myclient, (uint8_t*)"198.41.30.241", 1883 , 0);
	//MQTT_InitConnection(&mqttClient, "192.168.11.128", 1883, 0);

	MQTT_InitClient(&myclient, (uint8_t*) "lens_tX0vTNPfuNB8q2ztTywn1yRLGtO",(uint8_t*)"rabby25",
			(uint8_t*)"rabby25227", 120, 1);
	//MQTT_InitClient(&mqttClient, "client_id", "user", "pass", 120, 1);
	MQTT_InitLWT(&myclient, (uint8_t*)"/test" ,(uint8_t*)"offline" , 0, 0);
	MQTT_OnConnected(&myclient, this->onConnectedcb);
	MQTT_OnDisconnected(&myclient, this->onDisconnectedcd);
	MQTT_OnPublished(&myclient, this->publishedCb);
	MQTT_OnData(&myclient, this->dataCb);

	os_timer_disarm(&mqttSendF);
	os_timer_setfn(&mqttSendF, (os_timer_func_t *)timerCallback, NULL);
	os_timer_arm(&mqttSendF, 1000, 1);
}

ICACHE_FLASH_ATTR MqttManager::~MqttManager() {
}


void ICACHE_FLASH_ATTR MqttManager::
onConnectedcb(uint32_t *arg)
{
	MQTT_Client* client = (MQTT_Client*)arg;
	//os_printf("MQTT: Connected\r\n");
	MQTT_Subscribe(&myclient,(char*)"test/setting", 0);

	MQTT_Publish(&myclient,"/test/topic/0" ,"hello", 5, 0, 0);
}

void ICACHE_FLASH_ATTR MqttManager::
onDisconnectedcd(uint32_t *arg)
{
	MQTT_Client* client = (MQTT_Client*)arg;
	//os_printf("MQTT: Disconnected\r\n");
}

void ICACHE_FLASH_ATTR MqttManager::
publishedCb(uint32_t *arg)
{
	MQTT_Client* client = (MQTT_Client*)arg;
	//os_printf("MQTT: Published\r\n");
}

void ICACHE_FLASH_ATTR MqttManager::
dataCb(uint32_t *args, const char* topic,
		uint32_t topic_len, const char *data, uint32_t data_len)
{
	char *topicBuf = new char[topic_len+1];
	char *dataBuf = new char[data_len+1];

	os_memcpy(topicBuf, topic, topic_len);
	topicBuf[topic_len] = 0;

	os_memcpy(dataBuf, data, data_len);
	dataBuf[data_len] = 0;
	os_printf("Receive topic: %s, data: %s \r\n", topicBuf, dataBuf);


	delete[] topicBuf;
	delete[] dataBuf;

}


void ICACHE_FLASH_ATTR MqttManager::
connect()
{
	MQTT_Connect(&myclient);
	os_printf("Connecting mqtt\r\n");
}

void ICACHE_FLASH_ATTR MqttManager::
disconnect()
{
	MQTT_Disconnect(&myclient);
}

extern bool isWifiConnected;

void ICACHE_FLASH_ATTR MqttManager::
update(PMS3003Data *dataObj)
{
	if(isWifiConnected){
		//if(sendFlag == true){
			char *pbuf = NULL;
			pbuf = new char[jsonSize];
			os_memset(pbuf,'\0',jsonSize);
			json_ws_send((struct jsontree_value *)&sensorinfo_tree, "sensorData", pbuf);
			MQTT_Publish(&myclient,"/apl/pmSensor/JsonData", pbuf, strlen(pbuf), 0, 0);
			delete[] pbuf;
			//os_timer_setfn(&mqttSendF, (os_timer_func_t *)timerCallback, NULL);
			//os_timer_arm(&mqttSendF, 1000, 1);
			//sendFlag = false;
		//}

	}

}



