/*
	The hello world c++ demo
*/

#include <ets_sys.h>
#include <osapi.h>
#include <os_type.h>
#include <gpio.h>
#include <vector>
#include "routines.h"
#include "driver/Adafruit_ST7735.h"
#include "../pmSensor/PMS3003_Manager.h"
#include "../displayManager/DisplayManager.h"
#include "../mqtt/MqttManager.h"

#define DELAY 1000 /* milliseconds */
LOCAL uint8 secCounter;
LOCAL bool smartConfigFlag;
// =============================================================================================
// C includes and declarations
// =============================================================================================
extern "C"
{
#include <osapi.h>
#include <user_interface.h>
#include "driver/uart.h"
#include "espmissingincludes.h"
#include "../modules/include/wifi.h"
#include "smartconfig.h"
#include "user_json.h"
#include "config.h"
#include "ets_sys.h"
#include "os_type.h"
// declare lib methods
extern int ets_uart_printf(const char *fmt, ...);
extern void uart_setHook(void(*funcPtr)(uint8_t *buf));
extern void sntpClient_Init();
//void ets_timer_disarm(ETSTimer *ptimer);
//void ets_timer_setfn(ETSTimer *ptimer, ETSTimerFunc *pfunction, void *parg);
//void ets_timer_arm_new(ETSTimer *ptimer,uint32_t milliseconds, bool repeat_flag, bool);

}//extern "C"
// =============================================================================================
// global objects
// =============================================================================================
LOCAL os_timer_t setupTimer;
PMS3003_Manager pms3003Manager;
MqttManager myMqttManager;
Adafruit_ST7735 tft;
DisplayManager displayManager(&tft);
PMS3003Data pms3003Data;
bool isWifiConnected(false);

void smartconfig_done(sc_status status, void *pdata);
// =============================================================================================
// User code
// =============================================================================================
void ICACHE_FLASH_ATTR
wifiConnectCb(uint8_t status)
{
	if(status == STATION_GOT_IP){
		isWifiConnected = true;
		myMqttManager.connect();
	} else {
		isWifiConnected = false;
		myMqttManager.disconnect();
	}
}

LOCAL void ICACHE_FLASH_ATTR setupCallBack(void *arg)
{
	//os_printf("Start Timer Callback, %d\r\n",secCounter);
	secCounter++;
	if(secCounter >= 15){
		secCounter = 0;
		os_timer_disarm(&setupTimer);
		CFG_Load();
		if(!smartConfigFlag){
			os_printf("Smart Config TimeOut\r\nStopping Smart Config\r\n");
			if(sysCfg.flag == 1){
				smartconfig_stop();
				os_printf("\r\nPrevious ssid-> %s\r\n",sysCfg.sta_ssid);
				os_printf("\r\nPrevious pass-> %s\r\n",sysCfg.sta_pwd);
				WIFI_Connect(sysCfg.sta_ssid, sysCfg.sta_pwd, wifiConnectCb);

			}
		}

	}

}

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABBBCDDD
 *                A : rf cal
 *                B : at parameters
 *                C : rf init data
 *                D : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
*******************************************************************************/
extern "C" uint32 ICACHE_FLASH_ATTR user_rf_cal_sector_set(void)
{
    enum flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 8;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}

extern "C" void ICACHE_FLASH_ATTR user_rf_pre_init(void)
{
}

void ICACHE_FLASH_ATTR
smartconfig_done(sc_status status, void *pdata)
{
    switch(status) {
        case SC_STATUS_WAIT:
            os_printf("SC_STATUS_WAIT\n");
            break;
        case SC_STATUS_FIND_CHANNEL:
            os_printf("SC_STATUS_FIND_CHANNEL\n");
            break;
        case SC_STATUS_GETTING_SSID_PSWD:
            os_printf("SC_STATUS_GETTING_SSID_PSWD\n");
            os_timer_disarm(&setupTimer);
            break;
        case SC_STATUS_LINK:
            os_printf("SC_STATUS_LINK\n");
            struct station_config *sta_conf;
            sta_conf= (struct station_config*)pdata;
            os_printf("Getting SSID and Password\r\n");
            smartConfigFlag = true;
            CFG_Save(sta_conf);
            WIFI_Connect(sta_conf->ssid, sta_conf->password, wifiConnectCb);
            break;
        case SC_STATUS_LINK_OVER:
            os_printf("SC_STATUS_LINK_OVER\n");
            secCounter = 12;
            os_timer_setfn(&setupTimer, (os_timer_func_t *)setupCallBack, (void *)0);
            os_timer_arm(&setupTimer, DELAY, 1);
            smartconfig_stop();
            break;
    }

}


extern "C" void ICACHE_FLASH_ATTR user_init(void)
{
	do_global_ctors();
	secCounter = 0;
	smartConfigFlag = false;
	sntpClient_Init();
	//system_update_cpu_freq(SYS_CPU_160MHZ);
	// Configure the UART
	uart_init(BIT_RATE_9600, BIT_RATE_9600);
	os_delay_us(1000);

	smartconfig_set_type(SC_TYPE_ESPTOUCH); //SC_TYPE_ESPTOUCH,SC_TYPE_AIRKISS,SC_TYPE_ESPTOUCH_AIRKISS
	wifi_set_opmode(STATION_MODE);
	smartconfig_start(smartconfig_done);
	//os_printf("Start Smart Config\r\n");

	uart_setHook(static_cast<FuncPtr>(pms3003Manager.parseAndUpdate));
	pms3003Manager.registerLisenter(&myMqttManager);
	pms3003Manager.registerLisenter(&displayManager);
	//os_printf("SDK version-> %s\r\n",system_get_sdk_version());
	//os_printf("\r\nSystem started ...\r\n");

	os_timer_disarm(&setupTimer);
	os_timer_setfn(&setupTimer, (os_timer_func_t *)setupCallBack, (void *)0);
	os_timer_arm(&setupTimer, DELAY, 1);
}
