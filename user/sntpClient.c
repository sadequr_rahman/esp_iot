/*
 * sntpClient.c
 *
 *  Created on: Jan 17, 2017
 *      Author: APL-SadequrRahman
 */

#include "c_types.h"
#include "user_interface.h"
#include "osapi.h"
#include "sntp.h"
#include "mem.h"




void ICACHE_FLASH_ATTR sntpClient_Init()
{
	ip_addr_t *addr = (ip_addr_t *)os_zalloc(sizeof(ip_addr_t));
	sntp_setservername(0, "nist.netservicesgroup.com"); // set server 0 by domain name
	sntp_setservername(1, "time-c.nist.gov"); // set server 1 by domain name
	//ipaddr_aton("210.72.145.44", addr);
	IP4_ADDR(addr,210,72,145,44);
	sntp_setserver(2, addr); // set server 2 by IP address
	sntp_stop();
	sntp_set_timezone(6);
	sntp_init();
	os_free(addr);

}




