/*
 * AqiCal.cpp
 *
 *  Created on: Dec 18, 2016
 *      Author: APL-SadequrRahman
 */

#include "AqiCal.h"


#define PURPLE		0x8010
#define MAROON		0x8000
#define YELLOW		0xFFE0
#define GREEN		0x0400
#define RED			0xF800
#define ORANGE      0xFD20

char* good = "GOOD";
char* moderate= "MODERATE";
char* unhealthyForSensativeGroup = "UNHEALTHY(SG)";
char* unhealthy = "UNHEALTHY";
char* veryUnhealthy = "Very UNHEALTHY";
char* hazardous = "HAZARDOUS";

LOCAL uint8 findMapIndex(uint16 _data);


LOCAL AQI_Mapping_Table_t pm25Table[MAP_SIZE] = {
		{0.0,12.0,0,50},
		{12.1,35.4,51,100},
		{35.5,55.4,101,150},
		{55.5,150.4,151,200},
		{150.5,250.4,201,300},
		{250.5,350.4,301,400},
		{350.5,500,401,500}
};


uint16 ICACHE_FLASH_ATTR calculateAQI(uint16 _data){
	uint16 result = 0;
	uint8 index = findMapIndex(_data);
	if(index == 255)
		return 501;

	real32 numerator  = ((real32)_data - pm25Table[index].AvgMin) * (pm25Table[index].AqiMax - pm25Table[index].AqiMin);
	real32 denominator = (pm25Table[index].AvgMax - pm25Table[index].AvgMin);
	result = (uint16)(numerator / denominator);
	result += pm25Table[index].AqiMin;
	if(result > 500)
		result = 500;
	return result;
}


uint8 ICACHE_FLASH_ATTR findMapIndex(uint16 _data)
{
	uint8 i;
	if(_data >= 500)
		_data = 500;

	for (i = 0; i < MAP_SIZE; ++i) {
		if (_data >= pm25Table[i].AvgMin && _data <= pm25Table[i].AvgMax) {
			return i;
		}
	}
	return 255;
}

char* ICACHE_FLASH_ATTR findStatus(uint16 _data){
	uint8 i;
	if(_data >= 500)
		_data = 500;

	for (i = 0; i < MAP_SIZE; ++i) {
		if (_data >= pm25Table[i].AqiMin && _data <= pm25Table[i].AqiMax) {
			switch (i) {
				case 0:
					return good;
					break;
				case 1:
					return moderate;
					break;
				case 2:
					return unhealthyForSensativeGroup;
					break;
				case 3:
					return unhealthy;
					break;
				case 4:
					return veryUnhealthy;
					break;
				case 5:
				case 6:
					return hazardous;
					break;
				default:
					return 0;
					break;
			}
		}
	}
}


uint16 findInexColor(uint16 idx){
int i;
for (i = 0; i < MAP_SIZE; ++i) {
		if (idx >= pm25Table[i].AqiMin && idx <= pm25Table[i].AqiMax) {
			switch (i) {
				case 0:
					return GREEN;
					break;
				case 1:
					return YELLOW;
					break;
				case 2:
					return ORANGE;
					break;
				case 3:
					return RED;
					break;
				case 4:
					return PURPLE;
					break;
				case 5:
				case 6:
					return MAROON;
					break;
				default:
					return 0;
					break;
			}
		}
	}

	return 0;
}
