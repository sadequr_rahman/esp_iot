/*
 * AqiCal.h
 *
 *  Created on: Dec 18, 2016
 *      Author: APL-SadequrRahman
 */

#ifndef AQICAL_AQICAL_H_
#define AQICAL_AQICAL_H_

#include "c_types.h"


#define MAP_SIZE 7


typedef struct {
	real32 AvgMin;
	real32 AvgMax;
	uint16 AqiMin;
	uint16 AqiMax;
}AQI_Mapping_Table_t;

uint16 calculateAQI(uint16 _data);
char* findStatus(uint16 _data);
uint16 findInexColor(uint16 idx);

#endif /* AQICAL_AQICAL_H_ */
