/*
 * BatteryLisenter.h
 *
 *  Created on: Jan 11, 2017
 *      Author: APL-SadequrRahman
 */

#ifndef BATTERY_BATTERYLISENTER_H_
#define BATTERY_BATTERYLISENTER_H_

class BatteryLisenter {
public:
	BatteryLisenter();
	virtual ~BatteryLisenter();

	virtual void OnChargingStateChange(bool _state) = 0;
	virtual void OnBatteryVoltChange(float _mV) = 0;
};

#endif /* BATTERY_BATTERYLISENTER_H_ */
