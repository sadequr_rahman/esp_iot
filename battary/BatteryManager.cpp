/*
 * BatteryManager.cpp
 *
 *  Created on: Jan 11, 2017
 *      Author: APL-SadequrRahman
 */

#include "BatteryManager.h"

extern "C" {
#include "c_types.h"
#include "user_interface.h"
#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
}

BatteryManager SystmeBatteryManager;

LOCAL os_timer_t batteryReadTime;
LOCAL os_timer_t notifyTimer;
LOCAL os_timer_t debonceTimer;
LOCAL const uint8_t debounce = 100;
LOCAL const uint16 delay = 3000;
LOCAL const uint16 notifyTime = 500;
LOCAL const uint16 adc_num = 5;
LOCAL const uint16 adc_clk_div = 8;
LOCAL const uint8 statePin = 5;

LOCAL void notifyCallback(void *args){
		SystmeBatteryManager.batteryModel.setMilliVolt(*((float*)args));
		SystmeBatteryManager.notifyVolt(*((float*)args));
}

LOCAL void batteryCallback(void *args){
	ETS_INTR_LOCK(); //close interrupt
	uint16 adc_addr[5] = {0,0,0,0,0};
	uint32 sum(0);
	system_adc_read_fast(adc_addr, adc_num, adc_clk_div);
	for(uint8 i = 0; i < adc_num ; i++){
		sum += adc_addr[i];
	}
	sum = (sum/adc_num);
	// TO DO multiply with battry voltage
	float res = ((float)(sum/adc_num) * 0.0029);
	os_timer_disarm(&notifyTimer);
	os_timer_setfn(&notifyTimer,notifyCallback,(void*)res);
	os_timer_arm(&notifyTimer,notifyTime,0);
	ETS_INTR_UNLOCK(); //open interrupt
	os_timer_setfn(&batteryReadTime,batteryCallback,(void*)0);
	os_timer_arm(&batteryReadTime,delay,0);
}

LOCAL void checkAndUpdateState(void *args)
{
	uint8_t level;
	level = GPIO_INPUT_GET(GPIO_ID_PIN(statePin));
	if(!level) {
		//pin is low
		os_printf("Pin Low\r\n");
		SystmeBatteryManager.notifyState(true);

	 }
	 else{
		 //pin is high
		os_printf("Pin High\r\n");
		SystmeBatteryManager.notifyState(false);
	 }
	ETS_GPIO_INTR_ENABLE();
}

LOCAL void intrHandlerCB(uint32 interruptMask,void *arg)
{
	ETS_GPIO_INTR_DISABLE();
	os_printf("Pin Interrupted\r\n");
	uint32_t gpio_status = GPIO_REG_READ(GPIO_STATUS_ADDRESS);
	os_timer_disarm(&debonceTimer);
	os_timer_setfn(&debonceTimer, (os_timer_func_t *)checkAndUpdateState, (void *)0);
	os_timer_arm(&debonceTimer, debounce, 0);
	gpio_intr_ack(interruptMask);
	//clear interrupt status
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpio_status);

}

BatteryManager::BatteryManager() {
	// TODO Auto-generated constructor stub
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U,FUNC_GPIO5);
	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO5_U);
	GPIO_DIS_OUTPUT(GPIO_ID_PIN(statePin));
	uint8_t level;
	level = GPIO_INPUT_GET(GPIO_ID_PIN(statePin));
	if(!level) {
		//pin is low
		this->batteryModel.setChargingState(true);

	 }
	 else{
		 //pin is high
		 this->batteryModel.setChargingState(false);
	 }
	ETS_GPIO_INTR_DISABLE();
	ETS_GPIO_INTR_ATTACH(intrHandlerCB, (void*)0);
	gpio_pin_intr_state_set(GPIO_ID_PIN(statePin),GPIO_PIN_INTR_ANYEDGE);
	ETS_GPIO_INTR_ENABLE();
	os_timer_disarm(&batteryReadTime);
	os_timer_setfn(&batteryReadTime,batteryCallback,(void*)0);
	os_timer_arm(&batteryReadTime,delay,1);

}

BatteryManager::~BatteryManager() {
	// TODO Auto-generated destructor stub
}

void ICACHE_FLASH_ATTR BatteryManager::registerLisenter(BatteryLisenter *Obj){
		this->LisenterList.push_back(Obj);
}

void ICACHE_FLASH_ATTR BatteryManager::notifyState(bool state){
	for (int i = 0; i < this->LisenterList.size(); ++i) {
			LisenterList[i]->OnChargingStateChange(state);
	}
}

void ICACHE_FLASH_ATTR BatteryManager::notifyVolt(float _mV){
	for (int i = 0; i < this->LisenterList.size(); ++i) {
				LisenterList[i]->OnBatteryVoltChange(_mV);
		}
}

