/*
 * BatteryManager.h
 *
 *  Created on: Jan 11, 2017
 *      Author: APL-SadequrRahman
 */

#ifndef BATTERY_BATTERYMANAGER_H_
#define BATTERY_BATTERYMANAGER_H_

#include <vector>
#include <list>

#include "BatteryModel.h"
#include "BatteryLisenter.h"

class BatteryManager {
public:
	BatteryManager();
	virtual ~BatteryManager();
	void registerLisenter(BatteryLisenter *Obj);
	void notifyState(bool state);
	void notifyVolt(float _mv);

	BatteryModel batteryModel;

private:
	std::vector <BatteryLisenter*>LisenterList;


};

#endif /* BATTERY_BATTERYMANAGER_H_ */
