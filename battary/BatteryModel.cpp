/*
 * BattaryModel.cpp
 *
 *  Created on: Jan 11, 2017
 *      Author: APL-SadequrRahman
 */

#include "BatteryModel.h"


extern "C"{
#include "c_types.h"
}

ICACHE_FLASH_ATTR BatteryModel::BatteryModel():
chargeState(false),
milliVolt(0.00)
{

}

ICACHE_FLASH_ATTR BatteryModel::~BatteryModel() {
	// TODO Auto-generated destructor stub
}

float ICACHE_FLASH_ATTR BatteryModel::getMilliVolt(void){
	return this->milliVolt;
}

 bool ICACHE_FLASH_ATTR BatteryModel::getChargingState(){
	return this->chargeState;
}

 void ICACHE_FLASH_ATTR BatteryModel::setMilliVolt(float _value){
 		this->milliVolt = _value;
 }

 void ICACHE_FLASH_ATTR BatteryModel::setChargingState(bool _state){
		this->chargeState = _state;
}

