/*
 * BattaryModel.h
 *
 *  Created on: Jan 11, 2017
 *      Author: APL-SadequrRahman
 */

#ifndef BATTARY_BATTERYMODEL_H_
#define BATTARY_BATTERYMODEL_H_

class BatteryModel {
public:
	BatteryModel();
	virtual ~BatteryModel();

	float getMilliVolt(void);
	bool getChargingState(void);

	void setMilliVolt(float _value);
	void setChargingState(bool _state);


private:
	float milliVolt;
	bool chargeState;
};

#endif /* BATTARY_BATTERYMODEL_H_ */
