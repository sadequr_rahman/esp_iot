/*
 * MACQ.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: APL-SadequrRahman
 */

#include "MACQ.h"

extern "C" {
#include "espmissingincludes.h"
}


ICACHE_FLASH_ATTR MACQ::MACQ(){
	// TODO Auto-generated constructor stub

}

ICACHE_FLASH_ATTR MACQ::~MACQ() {
	// TODO Auto-generated destructor stub
}

void ICACHE_FLASH_ATTR MACQ::put(uint8_t *_data){
	int i;
	PMS3003Data *d = new PMS3003Data();
	d->updateModel(_data);
	for (i = (Q_SIZE-1); i > 0; --i) {
		this->buf10_tsi[i] = this->buf10_tsi[i-1];
		this->buf25_tsi[i] = this->buf25_tsi[i-1];
		this->buf100_tsi[i] = this->buf100_tsi[i-1];
		this->buf10_atm[i] = this->buf10_atm[i-1];
		this->buf25_atm[i] = this->buf25_atm[i-1];
		this->buf100_atm[i] = this->buf100_atm[i-1];
	}
	this->buf10_tsi[0] = d->getPm010Tsi();
	this->buf25_tsi[0] = d->getPm025Tsi();
	this->buf100_tsi[0] = d->getPm100Tsi();
	this->buf10_atm[0] = d->getPm010Atm();
	this->buf25_atm[0] = d->getPm025Atm();
	this->buf100_atm[0] = d->getPm100Atm();
	delete d;
}

void ICACHE_FLASH_ATTR MACQ::get(PMS3003Data *_data){
	uint32_t result[6];
	ets_memset(result,0,sizeof(uint32_t)*6); // possible solution for unwanted high values or false values
	uint8_t resultIndex = 0, dataIndex = 0;
	for (resultIndex = 0; resultIndex < 6; ++resultIndex) {
		for (dataIndex = 0; dataIndex < Q_SIZE; dataIndex++) {
				switch (resultIndex) {
					case 0:
						result[resultIndex] += this->buf10_tsi[dataIndex];
						break;
					case 1:
						result[resultIndex] += this->buf25_tsi[dataIndex];
						break;
					case 2:
						result[resultIndex] += this->buf100_tsi[dataIndex];
						break;
					case 3:
						result[resultIndex] += this->buf10_atm[dataIndex];
						break;
					case 4:
						result[resultIndex] += this->buf25_atm[dataIndex];
						break;
					case 5:
						result[resultIndex] += this->buf100_atm[dataIndex];
						break;
					default:
						break;
				}
			}
	}

	_data->setPm010Tsi((uint16_t)result[0]/Q_SIZE);
	_data->setPm025Tsi((uint16_t)result[1]/Q_SIZE);
	_data->setPm100Tsi((uint16_t)result[2]/Q_SIZE);

	_data->setPm010Atm((uint16_t)result[3]/Q_SIZE);
	_data->setPm025Atm((uint16_t)result[4]/Q_SIZE);
	_data->setPm100Atm((uint16_t)result[5]/Q_SIZE);
}
