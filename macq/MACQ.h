/*
 * MACQ.h
 *
 *  Created on: Dec 12, 2016
 *      Author: APL-SadequrRahman
 */

#ifndef MACQ_MACQ_H_
#define MACQ_MACQ_H_

#include "../pmSensor/PMS3003Data.h"

extern "C" {
#include "c_types.h"
}

ICACHE_FLASH_ATTR const uint8_t Q_SIZE = 8;

class MACQ {
public:
	MACQ();
	virtual ~MACQ();

	void put(uint8_t *_data);
	void get(PMS3003Data *_data);

private:
	uint16_t buf25_tsi[Q_SIZE];
	uint16_t buf10_tsi[Q_SIZE];
	uint16_t buf100_tsi[Q_SIZE];
	uint16_t buf25_atm[Q_SIZE];
	uint16_t buf10_atm[Q_SIZE];
	uint16_t buf100_atm[Q_SIZE];
};

#endif /* MACQ_MACQ_H_ */
