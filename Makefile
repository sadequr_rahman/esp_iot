# Main settings includes
include	c:/Espressif/examples/ESP8266/settings.mk

# Individual project settings (Optional)
#BOOT		= new
#APP		= 1
#SPI_SPEED	= 40
SPI_MODE	= QIO
SPI_SIZE_MAP = 6
ESPPORT		= COM11
#ESPBAUD	= 256000

# Basic project settings
MODULES	= include/driver pmSensor mqtt mqtt/mqtt_c displayManager modules driver user macq aqiCal json battery
LIBS	= c gcc hal phy pp net80211 lwip wpa crypto main smartconfig ssl json

# Root includes
include	c:/Espressif/examples/ESP8266/common_cpp.mk
