/*
 * StatusBar.h
 *
 *  Created on: Dec 24, 2016
 *      Author: APL-SadequrRahman
 */

#ifndef DISPLAYMANAGER_STATUSBAR_H_
#define DISPLAYMANAGER_STATUSBAR_H_


#include <inttypes.h>


extern "C"{
#include <c_types.h>
#include "driver/Adafruit_ST7735.h"
}


class StatusBar {
public:
	StatusBar(Adafruit_ST7735& tft);
	StatusBar(Adafruit_ST7735& tft,uint8 height,uint8 width,uint8 x,uint8 y,
			uint16 barColor,uint16 msgColor,const char* txt);
	virtual ~StatusBar();

	void setMsgColor(uint16 msgColor);
	void setBarColor(uint16 barColor);
	void setMsg(const char *txt);
	void putOnScreen(void);
	void putOnScreen(uint8 x,uint8 y);
	void setHight(uint8 h);
	void setWidth(uint8 w);
	void clearBar(void);
	void invalidate(void);

private:
	Adafruit_ST7735 screen;
	char _txt[20];
	uint16 _barColor;
	uint16 _msgColor;
	uint8 _hight;
	uint8 _width;
	uint8 _x;
	uint8 _y;
};
#endif /* DISPLAYMANAGER_STATUSBAR_H_ */
