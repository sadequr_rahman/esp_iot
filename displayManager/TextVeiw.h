/*
 * TextVeiw.h
 *
 *  Created on: Dec 25, 2016
 *      Author: APL-SadequrRahman
 */

#ifndef DISPLAYMANAGER_TEXTVEIW_H_
#define DISPLAYMANAGER_TEXTVEIW_H_

#include <inttypes.h>
extern "C"{
#include <c_types.h>
#include "driver/Adafruit_ST7735.h"
}

typedef enum{
	small_txt = 2,
	medium_txt = 4,
	large_txt = 6
}text_size_t;

class TextVeiw {
public:
	TextVeiw(Adafruit_ST7735& tft);
	virtual ~TextVeiw();

	void setTextColor(uint16 txtColor);
	void setText(const char* txt);
	void setSize(text_size_t _size);
	void putOnScreen(uint8 x, uint8 y);
	void putOnScreen(void);

private:
	Adafruit_ST7735 screen;
	uint8 _x;
	uint8 _y;
	uint8 _txtSize;
	uint16 _txtColor;
	char _txt[16];
};

#endif /* DISPLAYMANAGER_TEXTVEIW_H_ */
