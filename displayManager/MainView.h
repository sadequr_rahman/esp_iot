/*
 * MainView.h
 *
 *  Created on: Dec 25, 2016
 *      Author: APL-SadequrRahman
 */

#ifndef DISPLAYMANAGER_MAINVIEW_H_
#define DISPLAYMANAGER_MAINVIEW_H_

class MainView {
public:
	MainView();
	virtual ~MainView();

	void setupView(void);
};

#endif /* DISPLAYMANAGER_MAINVIEW_H_ */
