/*
 * StatusBar.cpp
 *
 *  Created on: Dec 24, 2016
 *      Author: APL-SadequrRahman
 */

#include "StatusBar.h"

extern "C" {
#include "espmissingincludes.h"
}

ICACHE_FLASH_ATTR StatusBar::StatusBar(Adafruit_ST7735& tft):
screen(tft),_hight(0),_width(0),_x(0),_y(0),_msgColor(ST7735_WHITE),_barColor(ST7735_WHITE)
{
	ets_memset(this->_txt,0,sizeof(this->_txt));
}

ICACHE_FLASH_ATTR StatusBar::StatusBar(Adafruit_ST7735& tft,uint8 height,uint8 width,uint8 x,uint8 y,
					uint16 barColor,uint16 msgColor,const char* txt):
screen(tft),_hight(height),_width(width),_x(x),_y(y),_msgColor(ST7735_WHITE),_barColor(ST7735_WHITE)
{
	// TODO Auto-generated constructor stub
	ets_memcpy(this->_txt,txt,strlen(txt));



}

ICACHE_FLASH_ATTR StatusBar::~StatusBar() {
	// TODO Auto-generated destructor stub
}


ICACHE_FLASH_ATTR void StatusBar::clearBar()
{
	this->screen.fillRect(this->_x,this->_y,this->_width,this->_hight,_barColor);
	ets_memset(this->_txt,0,sizeof(this->_txt));
}

ICACHE_FLASH_ATTR void StatusBar::putOnScreen(){
	this->screen.fillRect(this->_x,this->_y,this->_width,this->_hight,this->_barColor);
	this->screen.setTextColor(this->_msgColor,this->_barColor);
	this->screen.drawCentreString(this->_txt, (this->_width/2 )+ this->_x,(this->_hight/2) + (this->_y-6) ,2);
}

ICACHE_FLASH_ATTR void StatusBar::putOnScreen(uint8 x, uint8 y){
	this->_x = x;
	this->_y = y;
	this->putOnScreen();
}

ICACHE_FLASH_ATTR void StatusBar::setMsg(const char *txt){
	ets_memset(this->_txt,0,sizeof(this->_txt));
	ets_memcpy(this->_txt,txt,strlen(txt));
}

ICACHE_FLASH_ATTR void StatusBar::setMsgColor(uint16 msgColor){
	this->_msgColor = msgColor;
}
ICACHE_FLASH_ATTR void StatusBar::setBarColor(uint16 barColor){
	this->_barColor = barColor;
}

ICACHE_FLASH_ATTR void StatusBar::setHight(uint8 h)
{
	this->_hight = h;
}

ICACHE_FLASH_ATTR void StatusBar::setWidth(uint8 w)
{
	this->_width = w;
}


ICACHE_FLASH_ATTR void StatusBar::invalidate(){
		this->putOnScreen();
}

