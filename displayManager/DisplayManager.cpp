/*
 * DisplayManager.cpp
 *
 *  Created on: Nov 27, 2016
 *      Author: APL-SadequrRahman
 */

#include "DisplayManager.h"
#include "StatusBar.h"
#include "TextVeiw.h"
#include "string.h"



extern "C"{
#include "AqiCal.h"
#include "mem.h"
#include "espmissingincludes.h"
#include "user_interface.h"
}
StatusBar *testBar;
TextVeiw *standInfo;

ICACHE_FLASH_ATTR DisplayManager::DisplayManager(Adafruit_ST7735 *tft) : screen(tft)
{
	// TODO Auto-generated constructor stub
	// Initialize TFT
	screen->begin();
	screen->fillScreen(VGA_WHITE);
	screen->setRotation(3);
	screen->setTextColor(VGA_BLACK,VGA_WHITE);
	testBar = new StatusBar(*screen);
	testBar->setBarColor(VGA_GREEN);
	testBar->setMsgColor(VGA_BLACK);
	//testBar->setMsg("Very Unhealthy");
	testBar->setHight(30);
	testBar->setWidth(120);
	testBar->putOnScreen(20,80);

	standInfo = new TextVeiw(*screen);
	standInfo->setSize(small_txt);
	standInfo->setText("AQI (US)");
	standInfo->setTextColor(VGA_BLACK);
	standInfo->putOnScreen(5,0);

	//screen->drawCentreString("2.5�m",screen->width()/2,screen->height()/2,4);
	//this->drawPlaceholder(5,0,100,40,VGA_BLACK,"Pm010 ug/m3");
	//this->drawPlaceholder(5,42,100,40,VGA_BLACK,"Pm025 ug/m3");
	//this->drawPlaceholder(5,84,100,40,VGA_BLACK,"Pm100 ug/m3");
	//screen->drawBitmap(0,0,bitmapI,screen->width(),screen->height());

}

ICACHE_FLASH_ATTR DisplayManager::~DisplayManager() {
	// TODO Auto-generated destructor stub
}

void ICACHE_FLASH_ATTR DisplayManager::update(PMS3003Data *obj)
{
	//screen->drawNumber(obj->getPm010Tsi(),25,20,2);
	//screen->drawNumber(obj->getPm025Tsi(),25,62,2);
	//screen->drawNumber(obj->getPm100Tsi(),25,106,2);
	/*
	this->screen->fillScreen(VGA_WHITE);
	this->drawPlaceholder(5,0,100,40,VGA_BLACK,"AQI");
	uint16 idx = calculateAQI(obj->getPm025Tsi());
	if(idx > 500){
		idx = 500;
	}
	screen->drawNumber(idx,25,20,2);
	screen->drawCentreString(findStatus(idx),screen->height()/2,screen->width()/2,2);
	*/


	uint16 idx = calculateAQI(obj->getPm025Tsi());
		if(idx > 500){
			idx = 500;
		}
	screen->drawNumber(idx,35,35,large_txt);
	testBar->setBarColor(findInexColor(idx));
	testBar->setMsg(findStatus(idx));
	testBar->invalidate();


}

int ICACHE_FLASH_ATTR DisplayManager::drawPlaceholder(int x, int y, int width, int height,
		int bordercolor, const char* headertext)
{
	int headersize = 18;
	screen->drawRoundRect(x, y, width, height, 3, bordercolor);
	screen->drawCentreString(headertext, x + width / 2, y + 1, 2);
	screen->drawFastHLine(x, y + headersize, width, bordercolor);

	return y + headersize;
}
