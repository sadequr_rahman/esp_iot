/*
 * TextVeiw.cpp
 *
 *  Created on: Dec 25, 2016
 *      Author: APL-SadequrRahman
 */

#include "TextVeiw.h"
extern "C" {
#include "espmissingincludes.h"
}


TextVeiw::TextVeiw(Adafruit_ST7735& tft):
screen(tft),_x(0),_y(0),_txtSize(small_txt),_txtColor(ST7735_BLACK)
{
	// TODO Auto-generated constructor stub
	ets_memset(this->_txt,'\0',sizeof(this->_txt));

}

TextVeiw::~TextVeiw() {
	// TODO Auto-generated destructor stub
}

ICACHE_FLASH_ATTR void TextVeiw::setText(const char* txt){
	ets_memset(this->_txt,'\0',sizeof(this->_txt));
	ets_memcpy(this->_txt,txt,sizeof(this->_txt));
}

ICACHE_FLASH_ATTR void TextVeiw::setSize(text_size_t _size){
	this->_txtSize = _size;
}

ICACHE_FLASH_ATTR void TextVeiw::putOnScreen(void){
	this->screen.setTextColor(this->_txtColor);
	this->screen.drawString(this->_txt,this->_x,this->_y,this->_txtSize);
}

ICACHE_FLASH_ATTR void TextVeiw::putOnScreen(uint8 x,uint8 y){
	this->_x = x;
	this->_y =y;
	this->screen.drawString(this->_txt,this->_x,this->_y,this->_txtSize);

}

ICACHE_FLASH_ATTR void TextVeiw::setTextColor(uint16 txtColor){
	this->_txtColor = txtColor;
}
